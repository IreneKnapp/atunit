{-# LANGUAGE OverloadedStrings, TemplateHaskell #-}

module Main where

import           Snap
import           Snap.Snaplet.Heist
import qualified Data.ByteString.Char8 as B
import           Control.Lens hiding (index)

-- Create the App object

data App = App
 { _heist :: Snaplet (Heist App)
 }

makeLenses ''App

instance HasHeist App where
  heistLens = subSnaplet heist


-- Build a routing table that describes how URLs are handled

routes :: [(B.ByteString, Handler App App ())]
routes = [ ("/", index)
         ]

app :: SnapletInit App App
app = makeSnaplet "app" "hello world app." Nothing $ do
    h <- nestSnaplet "heist" heist $ heistInit "templates"
    addRoutes routes
    return $ App h

index :: Handler App App ()
index = render "index"


-- Startup the server with this configuration

main :: IO ()
main = do serveSnaplet defaultConfig app
