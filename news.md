#  News list!

Contains project news [hopefully] updated weekly in reverse chronological order [newest first]. 

For some sorts of news [the gitlab activity feed](https://gitlab.com/atunit/atunit) may be a better source.

## Week of September 8th

* **Events // Updates**
    * New issue!!! [Launch Basic Website](https://gitlab.com/atunit/atunit/issues/23)
    * [Haskell hello world app enters the repo](https://gitlab.com/atunit/atunit/issues/6)
    * [Discussion update on front end dev](https://gitlab.com/atunit/atunit/issues/18)
    * New words on [the UX feedback issue](https://gitlab.com/atunit/atunit/issues/12)
    * Disuccsion happens [on one of gittips more obscure rules](https://gitlab.com/atunit/atunit/issues/12#note_270341)
* **Priority issues this week**
    * [Launch Basic Website](https://gitlab.com/atunit/atunit/issues/23)
    * [Connecting snap to postgres](https://gitlab.com/atunit/atunit/issues/21)
    * [Information architecture feedback](https://gitlab.com/atunit/atunit/issues/22)
    * [Website design feedback](https://gitlab.com/atunit/atunit/issues/9)
    * [Feedback on the features overview](https://gitlab.com/atunit/atunit/issues/8)
* **Issues closed this week**
    * [Outreach Issue](https://gitlab.com/atunit/atunit/issues/20)
    * [Basic Haskell Web Framework Setup](https://gitlab.com/atunit/atunit/issues/6)

## Week of September 1st

* **Dicussion updates**
    * [@simonv3](https://gitlab.com/u/simonv3) parses feature requirements on the [UX thread](https://gitlab.com/atunit/atunit/issues/12#note_265158)
    * [Information architecture feedback thread opened](https://gitlab.com/atunit/atunit/issues/22)
    * [Web Design thread dicussion](https://gitlab.com/atunit/atunit/issues/9#note_265047)
    * [Naming thread gets updated](https://gitlab.com/atunit/atunit/issues/4#note_265906)
    * [Front end dev thread gets updated](https://gitlab.com/atunit/atunit/issues/18#note_265891)
* **[Irene Knapp](https://gitlab.com/u/IreneKnapp) becomes project technical lead!**
    * She'll be helping with technical decisions and assigning tasks to people
* **Priority issues this week are:**
    * [Obtaining more Haskell Programmers](https://gitlab.com/atunit/atunit/issues/20)
    * [Working on the design of our future website](https://gitlab.com/atunit/atunit/issues/9)
    * [Creating a launch features overview](https://gitlab.com/atunit/atunit/issues/8)
    * [Basic Haskell web framework setup](https://gitlab.com/atunit/atunit/issues/6)

## Week of August 25th

* **ATUnit's homebase moves from [assembla](http://assembla.com/spaces/atunit/) to [gitlab](https://gitlab.com/atunit/atunit)**
    * A vote was called on using assembla for our projected due to its UX / UI issues
    * The vote ended with gitlab having ~6 votes, and github with (+4/-3) 1 votes
    * All other services recieved 1 ~ 2 votes
    * The vote was held fairly informally inside the IRC room
    * Future votes should definitely be held via more formal mechanisms
* **gittip, which this project is "branched" from to some degree, renames itself to gratipay**
    * Formerly gittip now gratipay's announcement post was posted [on Medium](https://medium.com/gratipay-blog/gratitude-gratipay-ef24ad5e41f9)
    * (this bullet point is awaiting a group approved rebuttal)
* **Priority issues this week are:**
    * [Obtaining more Haskell Programmers](https://gitlab.com/atunit/atunit/issues/20)
    * [Working on the design of our future website](https://gitlab.com/atunit/atunit/issues/9)
    * [Creating a launch features overview](https://gitlab.com/atunit/atunit/issues/8)
    * [Going through the Haskell with Snap setup](https://gitlab.com/atunit/atunit/issues/6)
* **Reminders**
    * Our projects want **your** input! Yes, **you**! Create a gitlab account and start [commenting on our issues](https://gitlab.com/atunit/atunit/issues) today!